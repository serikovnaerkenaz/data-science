import sys
import matplotlib
matplotlib.use('Agg')

import pandas as pd
import matplotlib.pyplot as plt
from scipy import stats
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import train_test_split

dataset = pd.read_csv("data.csv", header=0, sep=",")

x = dataset[['Max_Pulse']]
y = dataset['Average_Pulse']

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size = 0.3, random_state = 100)

slr = LinearRegression()  
slr.fit(x_train, y_train)
y_pred_slr= slr.predict(x_test)

print("Prediction for test set: {}".format(y_pred_slr))

plt.scatter(x_test,y_test)
plt.plot(x_test, y_pred_slr, 'Red')
plt.ylim(ymin=50, ymax=200)
plt.xlim(xmin=90, xmax=180)
plt.xlabel("Max_Pulse")
plt.ylabel ("Average_Pulse")
plt.show()

plt.savefig(sys.stdout.buffer)
sys.stdout.flush()






